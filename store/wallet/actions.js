import TruffleContract from '@truffle/contract'
import Web3 from 'web3'
import AdoptionArtifact from '~/static/contracts/Adoption.json'

export default {
  initWallet(vuexContext) {
    return initWeb3()
      .then((account) => {
        vuexContext.commit('setAccount', account)

        getAdopted().then((result) => {
          vuexContext.commit('setAdopteds', result)
        })

        return account
      })
      .catch((error) => {
        return error
      })
  },
  unAdopt(vuexContext, { id }) {
    const account = vuexContext.rootState.wallet.account

    handleUnAdopt(account, id)
      .then((result) => {
        getAdopted().then((result) => {
          vuexContext.commit('setAdopteds', result)
        })
      })
      .catch((error) => {
        console.error(error) // eslint-disable-line no-console
      })
  },
  adopt(vuexContext, { id }) {
    const account = vuexContext.rootState.wallet.account

    handleAdopt(account, id)
      .then((result) => {
        getAdopted().then((result) => {
          vuexContext.commit('setAdopteds', result)
        })
      })
      .catch((error) => {
        console.error(error) // eslint-disable-line no-console
      })
  },
}

const App = {
  web3Provider: null,
  contracts: {},
  web3: {},
  accounts: null,
}

function initWeb3() {
  // Modern dapp browsers...
  return new Promise((resolve, reject) => {
    if (window.ethereum) {
      try {
        // Request account access
        window.ethereum
          .request({
            method: 'eth_requestAccounts',
          })
          .then((accounts) => {
            resolve({ provider: window.ethereum, accounts })
          })
          .catch((error) => {
            reject(error)
          })
      } catch (error) {
        reject(new Error('User denied account access'))
      }
    }
    // Legacy dapp browsers...
    else if (window.web3) {
      resolve({ provider: window.web3.currentProvider, accounts: null })
    }
    // If no injected web3 instance is detected, fall back to Ganache
    else {
      const provider = new Web3.providers.HttpProvider('http://localhost:7545')
      resolve({ provider, accounts: null })
    }
  })
    .then(({ provider, accounts }) => {
      App.web3Provider = provider
      App.accounts = accounts
      App.web3 = new Web3(provider)

      // init contract
      App.contracts.Adoption = TruffleContract(AdoptionArtifact)
      // Set the provider for our contract
      App.contracts.Adoption.setProvider(App.web3Provider)

      // return new Promise.resolve(accounts)
      return accounts[0]
    })
    .catch((error) => {
      return error
    })
}

function getAdopted() {
  return new Promise((resolve, reject) => {
    let adoptionInstance

    App.contracts.Adoption.deployed()
      .then(function (instance) {
        adoptionInstance = instance

        return adoptionInstance.getAdopters.call()
      })
      .then(function (adopters) {
        const list = []
        for (let i = 0; i < adopters.length; i++) {
          if (adopters[i] !== '0x0000000000000000000000000000000000000000') {
            list.push({
              id: i,
            })
          }
        }

        resolve(list)
      })
      .catch(function (err) {
        reject(err.message)
      })
  })
}

function handleAdopt(account, id) {
  return new Promise((resolve, reject) => {
    let adoptionInstance

    App.contracts.Adoption.deployed()
      .then(function (instance) {
        adoptionInstance = instance

        // Execute adopt as a transaction by sending account
        return adoptionInstance.adopt(id, { from: account })
      })
      .then(function (result) {
        resolve(result)
      })
      .catch(function (err) {
        reject(err.message)
      })
  })
}

function handleUnAdopt(account, id) {
  return new Promise((resolve, reject) => {
    let adoptionInstance

    App.contracts.Adoption.deployed()
      .then(function (instance) {
        adoptionInstance = instance

        // Execute adopt as a transaction by sending account
        return adoptionInstance.unAdopt(id, { from: account })
      })
      .then(function (result) {
        resolve(result)
      })
      .catch(function (err) {
        reject(err.message)
      })
  })
}
