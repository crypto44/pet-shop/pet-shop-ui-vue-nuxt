export default {
  setAccount(state, account) {
    state.account = account
    state.isEnable = true
  },
  clearAccount(state) {
    state.account = null
    state.isEnable = false
  },
  setAdopteds(state, adopteds) {
    state.adopteds = adopteds
  },
}
