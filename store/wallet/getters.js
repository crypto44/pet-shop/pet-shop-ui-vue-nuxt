export default {
  walletIsEnable(state) {
    return state.isEnable
  },
  getAccount(state) {
    return state.account
  },
  getAdopteds(state) {
    return state.adopteds
  },
}
